// gauss_OpenMP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <ctime>
#include <omp.h>

using namespace std;

int col_max(vector<vector<double>> &matrix, int col, int n);
vector<double>gauss_parallel(vector<vector<double>> &matrix, vector<double> &free_term_column, int n);
void triangulation(vector<vector<double>> &matrix, int n);

int main()
{
	for (int n = 500; n < 2500; n += 500) {
		double start_time = clock();
		vector<vector<double> > matrix(n);
		for (int i = 0; i < n; ++i) {
			srand(i);
			matrix[i].resize(n);
			for (int j = 0; j < n; ++j)
				matrix[i][j] = rand() % 100 + 1;
		}
		vector<double> column(n);
		for (int j = 0; j < n; ++j)
			column[j] = rand() % 100 + 1;
		try {
			cout << "Matrix size" << n << endl;
			vector<double> solution = gauss_parallel(matrix, column, n);
			cout << "OpenMP time:" << " " << (clock() - start_time) / CLOCKS_PER_SEC << endl;

		}
		catch (exception) {
			cout << "It`s impossible to solve this system using Gauss method. Please, try again";
		}
	}
}


vector<double>gauss_parallel(vector<vector<double>> &matrix, vector<double> &free_term_column, int n) {
	vector<double> solution(n);
	for (int i = 0; i < n; ++i) {
		matrix[i].push_back(free_term_column[i]);
	}
	triangulation(matrix, n);
	for (int i = n - 1; i >= 0; --i) {
		if (abs(matrix[i][i]) < 0.0001)
			throw new exception();
		for (int j = i + 1; j < n; ++j) {
			matrix[i][n] -= matrix[i][j] * solution[j];
		}
		solution[i] = matrix[i][n] / matrix[i][i];
	}
	return solution;
}

void triangulation(vector<vector<double>> &matrix, int n) {
	if (n == 0)
		return;
	const int num_cols = matrix[0].size();
	for (int i = 0; i < n - 1; ++i) {
		unsigned int imax = col_max(matrix, i, n);
		if (i != imax)
			swap(matrix[i], matrix[imax]);
		for (int j = i + 1; j < n; ++j) {
			double mul = -matrix[j][i] / matrix[i][i];
#pragma omp parallel for
			for (int k = i; k < n; ++k) {
				matrix[j][k] += matrix[i][k] * mul;
			}
		}
	}
}

int col_max(vector<vector<double>> &matrix, int col, int n) {
	double max = abs(matrix[col][col]);
	int maxPos = col;
#pragma omp parallel
	{
		double loc_max = max;
		int loc_max_pos = maxPos;
#pragma omp for
		for (int i = col + 1; i < n; ++i) {
			double element = abs(matrix[i][col]);
			if (element > loc_max) {
				loc_max = element;
				loc_max_pos = i;
			}
		}
#pragma omp critical
		{
			if (max < loc_max) {
				max = loc_max;
				maxPos = loc_max_pos;
			}
		}
	}
	return maxPos;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
