// gauss_MPI.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "mpi.h"
#include <vector>

using namespace std;

int get_chunk(int total, int commsize, int rank);

int main(int argc, char **argv)
{
	int rank, commsize;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &commsize);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	for (int n = 500; n < 2500; n += 500) {
		double t = MPI_Wtime();
		int nrows = get_chunk(n, commsize, rank);
		vector<double> rows(nrows);
		vector<vector<double> > a(nrows);
		vector<double> x(n);
		vector<double> tmp(n+1);
		for (int i = 0; i < nrows; i++) {
			a[i].resize(n + 1);
			rows[i] = rank + commsize * i; 
			srand(rows[i] * (n + 1)); 
			for (int j = 0; j < n; j++)
				a[i][j] = rand() % 100 + 1;
			// b[i]
			a[i][n] = rand() % 100 + 1;
		}
			// ������ ���
			int row = 0;
			for (int i = 0; i < n -1; i++) {
				// ��������� x_i
				if (i== rows[row]) {
					// ��������� ������ i, ����������� � ������ �������� ��������
					MPI_Bcast(&a[row], n + 1, MPI_DOUBLE, rank, MPI_COMM_WORLD);
					for (int j = 0; j <= n; j++)
						tmp[j] = a[row][j];
					row++;
				}
				else 
				{
					MPI_Bcast(&tmp, n + 1, MPI_DOUBLE, i% commsize, MPI_COMM_WORLD);
				}
				// �������� �������� ������ �� ���������, ���������� � ������� ��������
				for (int j = row; j < nrows; j++) {
					double scaling = a[j][i] / tmp[i];
					for (int k = i; k < n + 1; k++)
						a[j][k] -= scaling * tmp[k];
				}
			}
			// ������������� �����������
			row = 0;
			for (int i = 0; i < n; i++) {
				x[i] = 0;
				if (i== rows[row]) {
					x[i] = a[row][n];
					row++;}
			}
			/* �������� ��� */
			row = nrows - 1; 
			for (int i = n - 1; i > 0; i--) {
				if (row >= 0) {
					if (i == rows[row]) {
						x[i] /= a[row][i];// �������� ��������� x_i
						MPI_Bcast(&x[i], 1, MPI_DOUBLE, rank, MPI_COMM_WORLD);row--;
					} else
						MPI_Bcast(&x[i], 1, MPI_DOUBLE, i% commsize, MPI_COMM_WORLD);
				} else
					MPI_Bcast(&x[i], 1, MPI_DOUBLE, i% commsize, MPI_COMM_WORLD);
				for (int j = 0; j <= row; j++)                  // ������������� ��������� x_i
					x[rows[j]] -= a[j][i] * x[i];
			}
			if (rank == 0)
				x[0] /= a[0][row];                       // ������������� x_0
			MPI_Bcast(&x, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			// ��� �������� �������� ���������� ������ x[n] �������
			t = MPI_Wtime()-t;
			if (rank == 0) {
				printf("Matrix size: %d, procs %d, MPI time  %.6f sec\n", n, commsize, t);
				
			}

	}
	MPI_Finalize();
	return 0;
}

int get_chunk(int total, int commsize, int rank) { 
	int n = total;
	int q = n / commsize; 
	if (n % commsize)q++; 
	int r = commsize * q - n;/* Compute chunk size for the process */
	int chunk = q; 
	if (rank >= commsize - r)
		chunk = q - 1; 
	return chunk; 
}